.TH XECU 1 2019-06-25 "Linux" "xecu man page"

.SH NAME
   xecu - Create a custom menu in a popup window

.SH SYNOPSIS
   xecu [ -v ] [ name ]

.SH DESCRIPTION
   xecu is a popup window with menu items as icons and text. Click a menu
   item to launch the corresponding application. Menu items can be created
   using a GUI dialog or by dragging desktop files or data files into the
   xecu window. Arrange the layout by dragging menu items with the mouse.

.SH OPTIONS
   Command line options
      -v       print version and exit
      name     title for popup window and name for corresponding text file
               where the menu data is saved. Defaults to "menu.txt".

.SH OVERVIEW

Initialization and Startup
   Create a desktop icon/launcher using the command: $ xecu <name>.
   An empty window is created when xecu is first started.

Adding and Editing Menu Items
   To add a new menu item, right-click an empty space in the window and
   fill-in the popup dialog with menu text, menu function, and optional icon
   file. A shell command using the function will be issued when the menu is
   clicked. If there is no function, the menu text and icon appear in the
   window but do nothing. Use this for headings or other information.
   The menu text can have multiple lines up to 1000 characters. Use '\n'
   to separate lines. If "close window" is checked, the xecu window will
   vanish when the menu item is selected. To change a menu, right-click and
   edit the popup dialog containing the existing menu data. Delete a menu
   with the dialog [delete] button.

Adding Menu Items by Drag & Drop Desktop Icons/Launchers
   A desktop launcher is a text file (appname.desktop) which appears on the
   desktop as an icon which can be clicked to launch the application. These
   icons can be dragged onto the xecu window and will work the same way.
   The directory /usr/share/applications has desktop files for installed
   applications, and these can be dragged onto the xecu window.

Adding Menu Items by Drag & Drop File Names
   A file can be dragged onto the xecu window to become a clickable menu
   which starts the default application to open the file. Nautilus can be
   used as a source to drag files. The initial menu name is the file base
   name, which can be edited as described above. The icon is the application
   icon. For an image file, the icon is a thumbnail of the image.

Icon Size
   The default size is 32 pixels wide, but the dialog allows this to be
   changed to any value within 24-256 pixels. When the icon size is changed,
   the new value becomes the default for the next menu entry added in the
   same session.

Arranging the Layout
   The menu items may be dragged around the window using the mouse. They will
   register on 8-pixel boundaries to simplify alignment. Expand the window if
   more space is needed, and this size will be retained when the window is
   opened again.

Adding Multiple xecu Windows
   The initial xecu window may contain menu items which are links to other
   xecu windows. For the menu function, use "xecu xxxxx", where "xxxxx"
   is a unique text file name to contain the menu data. Clicking on this new
   menu item will bring up a new empty xecu window which can be populated
   with menu items like the initial window. These link menus can be used to
   build a circular list of windows with "next" and "back" links, or a
   hierarchy of parent-child windows.

Exit xecu
   To close the menu window, click the [x] cancel button.

.SH AUTHORS
 Originally released as "mystuff", written by Michael Cornelison ( https://www.kornelix.net )
