# xecu make file

# defaults for parameters that may be pre-defined
CXXFLAGS += -Wall -g -rdynamic -O2 -Wno-format-truncation
PREFIX ?= /usr

# target install directories
BINDIR = $(PREFIX)/bin
SHAREDIR = $(PREFIX)/share/xecu
DATADIR = $(SHAREDIR)/data
ICONDIR = $(SHAREDIR)/icons
IMAGEDIR = $(SHAREDIR)/images
LOCALESDIR = $(SHAREDIR)/locales
DOCDIR = $(PREFIX)/share/doc/xecu
MANDIR = $(PREFIX)/share/man/man1
MENUFILE = $(PREFIX)/share/applications/xecu.desktop

CFLAGS = $(CXXFLAGS) -c `pkg-config --cflags gtk+-3.0`
CFLAGS += $(CPPFLAGS)
LIBS = `pkg-config --libs gtk+-3.0` -lpthread

xecu: xecu.o zfuncs.o
	$(CXX) $(LDFLAGS) -o xecu xecu.o zfuncs.o $(LIBS)

xecu.o: xecu.cc
	$(CXX) $(CFLAGS) -o xecu.o xecu.cc

zfuncs.o: zfuncs.cc zfuncs.h
	$(CXX) $(CFLAGS) zfuncs.cc    \
          -D PREFIX=\"$(PREFIX)\" -D DOCDIR=\"$(DOCDIR)\"

install: xecu uninstall
	mkdir -p  $(DESTDIR)$(BINDIR)
	mkdir -p  $(DESTDIR)$(DATADIR)
	mkdir -p  $(DESTDIR)$(ICONDIR)
	mkdir -p  $(DESTDIR)$(IMAGEDIR)
	mkdir -p  $(DESTDIR)$(LOCALESDIR)
	mkdir -p  $(DESTDIR)$(DOCDIR)
	mkdir -p  $(DESTDIR)$(MANDIR)
	mkdir -p  $(DESTDIR)$(PREFIX)/share/applications
	cp -f  xecu $(DESTDIR)$(BINDIR)
	cp -f -R  data/* $(DESTDIR)$(DATADIR)
	cp -f -R  xecu.png $(DESTDIR)$(IMAGEDIR)
	cp -f -R  doc/* $(DESTDIR)$(DOCDIR)
	gzip -f -9 $(DESTDIR)$(DOCDIR)/changelog
	# man page
	cp -f doc/xecu.man xecu.1
	gzip -f -9 xecu.1
	cp xecu.1.gz $(DESTDIR)$(MANDIR)
	rm -f xecu.1.gz
	# menu (desktop) file
	cp -f xecu.desktop $(DESTDIR)$(MENUFILE)
	cp -f xecu.png $(DESTDIR)$(ICONDIR)

uninstall:
	rm -f  $(DESTDIR)$(BINDIR)/xecu
	rm -f -R  $(DESTDIR)$(SHAREDIR)
	rm -f -R  $(DESTDIR)$(DOCDIR)
	rm -f  $(DESTDIR)$(MANDIR)/xecu.1.gz
	rm -f  $(DESTDIR)$(MENUFILE)

clean:
	rm -f  xecu
	rm -f  *.o


