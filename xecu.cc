/**************************************************************************
   xecu - custom menu in popup window

   Copyright 2007-2019 Michael Cornelison
   source code URL: https://gitlab.com/skidoo/xecu

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version. See https://www.gnu.org/licenses

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU General Public License for more details.

***************************************************************************/

#include "zfuncs.h"
#define fversion "xecu 3.1"                                                   //  version

int main(int argc, char *argv[])
{
   void callbackfunc(cchar *menu);

   char     *file, menufile[200], title[100] = "";

   if (argc > 1 && strmatch(argv[1],"-v")) {
      printf(fversion "\n");                                                     //  print program and version
      exit(0);
   }

   appimage_install("xecu");                                                  //  if appimage, menu integration      3.1

   if (argc > 1 && strmatch(argv[1],"-uninstall"))                               //  uninstall appimage                 3.1
      appimage_unstall();                                                        //  (does not return if uninstalled)

   setenv("GDK_BACKEND","x11",1);                                                //  fedora/wayland
   setenv("GTK_THEME","default",0);                                              //  KDE window manager

   gtk_init(&argc,&argv);                                                        //  initz. GTK

   zinitapp("xecu");                                                          //  get app directories
   zsetfont("sans 10");                                                          //  2.6       howdy     WAS 10

   if (argc > 1) file = argv[1];                                                 //  multiple pages
   else file = (char *) "menu.txt";
   snprintf(menufile,199,"%s/%s",get_zhomedir(),file);                           //  menu file /home/<user>/.config/xecu/menu.txt

   strncatv(title,100,fversion," ",file,null);                                   //  2.6
   Gmenuz(null,title,menufile,callbackfunc);                                     //  create popup menu window

   gtk_main();                                                                   //  process window events
   return 0;
}


//  popup menu window calls this function when a menu is clicked
void callbackfunc(cchar *menu)
{
   if (strmatch(menu,"quit")) {
      gtk_main_quit();                                                           //  exit application
      return;
   }

   shell_ack("%s &",menu);                                                       //  run command
   return;
}


//  supply unused zdialog callback function
void KBevent(GdkEventKey *event)
{ return; }
